<?php
/**
 * Login Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce; ?>

<?php $woocommerce->show_messages(); ?>

<?php do_action('woocommerce_before_customer_login_form'); ?>

<div class="row myaccount-login">
	<div class="<?php echo sp_column_css( '', '', '', '6' ); ?>">
		<?php echo do_shortcode( '[sp-login redirect_to="' . get_permalink( woocommerce_get_page_id( 'myaccount' ) ) . '"]' ); ?>
	</div><!--close .column-->

	<div class="<?php echo sp_column_css( '', '', '', '6' ); ?>">
		<?php if (get_option('woocommerce_enable_myaccount_registration')=='yes') : ?>
			<?php echo do_shortcode( '[sp-register]' ); ?>
		<?php endif; ?>
	</div><!--close .column-->
</div><!--close .row-->

<?php do_action('woocommerce_after_customer_login_form'); ?>