<?php
/**
 * My Addresses
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

$customer_id = get_current_user_id();

if ( get_option('woocommerce_ship_to_billing_address_only') == 'no' ) {
	$page_title = apply_filters( 'woocommerce_my_account_my_address_title', __( 'My Addresses', 'sp-theme' ) );
	$get_addresses    = array(
		'billing' => __( 'Billing Address', 'sp-theme' ),
		'shipping' => __( 'Shipping Address', 'sp-theme' )
	);
} else {
	$page_title = apply_filters( 'woocommerce_my_account_my_address_title', __( 'My Address', 'sp-theme' ) );
	$get_addresses    = array(
		'billing' =>  __( 'Billing Address', 'sp-theme' )
	);
}

$col = 1;
?>

<h3 class="title-with-line"><span><?php echo $page_title; ?></span></h3>

<p class="myaccount_address">
	<?php echo apply_filters( 'woocommerce_my_account_my_address_description', __( 'The following addresses will be used on the checkout page by default.', 'sp-theme' ) ); ?>
</p>

<?php //if ( get_option('woocommerce_ship_to_billing_address_only') == 'no' ) echo '<div class="col2-set addresses">'; ?>

<div class="row">

<?php foreach ( $get_addresses as $name => $title ) : ?>

	<div class="<?php echo sp_column_css( '', '', '', '6' ); ?> address">
		<header class="title">
			<h3 class="title-with-line"><span><?php echo $title; ?></span></h3>
		</header>
		<address>
			<?php
				$address = apply_filters( 'woocommerce_my_account_my_address_formatted_address', array(
					'first_name' 	=> get_user_meta( $customer_id, $name . '_first_name', true ),
					'last_name'		=> get_user_meta( $customer_id, $name . '_last_name', true ),
					'company'		=> get_user_meta( $customer_id, $name . '_company', true ),
					'address_1'		=> get_user_meta( $customer_id, $name . '_address_1', true ),
					'address_2'		=> get_user_meta( $customer_id, $name . '_address_2', true ),
					'city'			=> get_user_meta( $customer_id, $name . '_city', true ),
					'state'			=> get_user_meta( $customer_id, $name . '_state', true ),
					'postcode'		=> get_user_meta( $customer_id, $name . '_postcode', true ),
					'country'		=> get_user_meta( $customer_id, $name . '_country', true )
				), $customer_id, $name );

				$formatted_address = $woocommerce->countries->get_formatted_address( $address );

				if ( ! $formatted_address )
					_e( 'You have not set up this type of address yet.', 'sp-theme' );
				else
					echo $formatted_address;
			?>
		</address>
		<a href="<?php echo esc_url( add_query_arg('address', $name, get_permalink(woocommerce_get_page_id( 'edit_address' ) ) ) ); ?>" class="edit-address" data-address-type="<?php echo esc_attr( $name ); ?>"><?php _e( 'Edit', 'sp-theme' ); ?></a>
	</div>

<?php endforeach; ?>
</div><!--close .row-->
<?php //if ( get_option('woocommerce_ship_to_billing_address_only') == 'no' ) echo '</div>'; ?>