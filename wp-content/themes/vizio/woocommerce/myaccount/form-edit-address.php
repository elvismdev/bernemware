<?php
/**
 * Edit address form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce, $current_user;

get_currentuserinfo();
?>

<?php $woocommerce->show_messages(); ?>

<?php sp_woo_checkout_additional_info(); ?>
<div class="row edit-address">
	
	<div class="<?php echo sp_column_css( '', '', '', '3' ); ?>">
		<nav class="account-nav">
			<h3 class="title-with-line"><span><?php _e( 'My Account', 'sp-theme' ); ?></span></h3>
			<ul>
				<li><a href="<?php echo get_permalink( woocommerce_get_page_id( 'myaccount' ) ); ?>" title="<?php esc_attr_e( 'Back to My Account', 'sp-theme' ); ?>" class="edit-address-link"><i class="icon-angle-left" aria-hidden="true"></i><?php _e( 'Back to My Account', 'sp-theme' ); ?></a></li>
			</ul>
		</nav>
	</div><!--close .column-->

	<div class="<?php echo sp_column_css( '', '', '', '9' ); ?>">

		<?php if (!$load_address) : ?>

			<?php woocommerce_get_template('myaccount/my-address.php'); ?>

		<?php else : ?>

			<form action="<?php echo esc_url( add_query_arg( 'address', $load_address, get_permalink( woocommerce_get_page_id('edit_address') ) ) ); ?>" method="post">

				<h3><?php if ($load_address=='billing') _e( 'Billing Address', 'sp-theme' ); else _e( 'Shipping Address', 'sp-theme' ); ?></h3>

				<?php
				foreach ($address as $key => $field) :
					$value = (isset($_POST[$key])) ? $_POST[$key] : get_user_meta( get_current_user_id(), $key, true );

					// Default values
					if (!$value && ($key=='billing_email' || $key=='shipping_email')) $value = $current_user->user_email;
					if (!$value && ($key=='billing_country' || $key=='shipping_country')) $value = $woocommerce->countries->get_base_country();
					if (!$value && ($key=='billing_state' || $key=='shipping_state')) $value = $woocommerce->countries->get_base_state();

					woocommerce_form_field( $key, $field, $value );
				endforeach;
				?>

				<p>
					<input type="submit" class="button" name="save_address" value="<?php _e( 'Save Address', 'sp-theme' ); ?>" />
					<?php $woocommerce->nonce_field('edit_address') ?>
					<input type="hidden" name="action" value="edit_address" />
				</p>

			</form>

		<?php endif; ?>
	</div><!--close .column-->
</div><!--close .row-->		