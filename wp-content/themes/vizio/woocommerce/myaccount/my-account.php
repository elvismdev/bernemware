<?php
/**
 * My Account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

$woocommerce->show_messages(); 

sp_woo_checkout_additional_info(); 
?>

<div class="row myaccount">
	
	<div class="<?php echo sp_column_css( '', '', '', '3' ); ?>">
		<nav class="account-nav">
			<h3 class="title-with-line"><span><?php _e( 'My Account', 'sp-theme' ); ?></span></h3>
			<ul>
				<?php echo apply_filters( 'sp_woo_myaccount_nav_orders', '<li><a href="#orders" title="' . esc_attr__( 'Recent Orders', 'sp-theme' ) . '">' . __( 'Recent Orders', 'sp-theme' ) . ' <i class="icon-angle-right" aria-hidden="true"></i></a></li>' ); ?>
				
				<?php
				// check if we need to show downloadables
				if ( $woocommerce->customer->get_downloadable_products() )
				?>
					<?php echo apply_filters( 'sp_woo_myaccount_nav_downloads', '<li><a href="#downloads" title="' . esc_attr__( 'My Downloads', 'sp-theme' ) . '">' . __( 'My Downloads', 'sp-theme' ) . ' <i class="icon-angle-right" aria-hidden="true"></i></a></li>' ); ?>

				<?php echo apply_filters( 'sp_woo_myaccount_nav_addresses', '<li><a href="#addresses" title="' . esc_attr__( 'My Addresses', 'sp-theme' ) . '">' . __( 'My Addresses', 'sp-theme' ) . ' <i class="icon-angle-right" aria-hidden="true"></i></a></li>' ); ?>

				<?php echo apply_filters( 'sp_woo_myaccount_nav_wishlist', '<li><a href="#wishlists" title="' . esc_attr__( 'My Wishlists', 'sp-theme' ) . '">' . __( 'My Wishlists', 'sp-theme' ) . ' <i class="icon-angle-right" aria-hidden="true"></i></a></li>' ); ?>

				<?php echo apply_filters( 'sp_woo_myaccount_nav_password', '<li><a href="#passwords" title="' . esc_attr__( 'Change Password', 'sp-theme' ) . '" class="get-password-page" data-src="' . get_permalink( woocommerce_get_page_id( 'change_password' ) ) . '">' . __( 'Change Password', 'sp-theme' ) . ' <i class="icon-angle-right" aria-hidden="true"></i></a></li>' ); ?>
			</ul>
		</nav>
	</div><!--close .column-->

	<div class="<?php echo sp_column_css( '', '', '', '9' ); ?>">

		<?php do_action( 'woocommerce_before_my_account' ); ?>
		
		<div id="orders">
		<?php woocommerce_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>
		</div><!--close #orders-->
		
		<div id="downloads">
		<?php woocommerce_get_template( 'myaccount/my-downloads.php' ); ?>
		</div><!--close .#downloads-->
		
		<div id="addresses">
		<?php woocommerce_get_template( 'myaccount/my-address.php' ); ?>
		</div><!--close #address-->
		
		<div id="wishlists">
		<?php sp_woo_add_product_wishlist_to_my_account(); ?>
		</div><!--close #wishlist-->
		
		<div id="passwords">
		<?php woocommerce_get_template( 'myaccount/form-change-password.php' ); ?>
		</div><!--close #passwords-->

		<?php do_action( 'woocommerce_after_my_account' ); ?>
	</div><!--close .column-->
</div><!--close .row-->