<?php
/**
 * Review order form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

$available_methods = $woocommerce->shipping->get_available_shipping_methods();
?>
<div id="order_review" class="clearfix">
	
	<div class="shop-table-container">
		<table class="shop_table">
			<thead>
				<tr>
					<th class="product-image"></th>
					<th class="product-name"><?php _e( 'Product', 'sp-theme' ); ?></th>
					<th class="product-unit-price"><?php _e( 'Unit Price', 'sp-theme' ); ?></th>
					<th class="product-quantity"><?php _e( 'Quantity', 'sp-theme' ); ?></th>
					<th class="product-subtotal"><?php _e( 'Subtotal', 'sp-theme' ); ?></th>
				</tr>
			</thead>

			<tbody>
				<?php
					do_action( 'woocommerce_review_order_before_cart_contents' );

					if (sizeof($woocommerce->cart->get_cart())>0) :
						foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) :
							$_product = $values['data'];
							$image = sp_get_image( get_post_thumbnail_id( $_product->id ), 60, 60, true );

							$product_price = get_option('woocommerce_tax_display_cart') == 'excl' ? $_product->get_price_excluding_tax() : $_product->get_price_including_tax();

							if ($_product->exists() && $values['quantity']>0) :
								echo '
									<tr class="' . esc_attr( apply_filters('woocommerce_checkout_table_item_class', 'checkout_table_item', $values, $cart_item_key ) ) . '">
										<td class="product-image"><img src="' . esc_url( $image['url'] ) . '" alt="' . esc_attr( $image['alt'] ) . '" /></td>
										<td class="product-name">' .
											apply_filters( 'woocommerce_checkout_product_title', $_product->get_title(), $_product ) . '<br />' .
											$woocommerce->cart->get_item_data( $values ) .
										'</td>
										<td class="product-price">' .
											apply_filters('woocommerce_cart_item_price_html', woocommerce_price( $product_price ), $values, $cart_item_key ) . 				
										'<td class="product-quantity">' .
											apply_filters( 'woocommerce_checkout_item_quantity', '<strong class="product-quantity">' . $values['quantity'] . '</strong>', $values, $cart_item_key ) .
										'<td class="product-total">' . apply_filters( 'woocommerce_checkout_item_subtotal', $woocommerce->cart->get_product_subtotal( $_product, $values['quantity'] ), $values, $cart_item_key ) . '</td>
									</tr>';
							endif;
						endforeach;
					endif;

					do_action( 'woocommerce_review_order_after_cart_contents' );
				?>
			</tbody>
		</table>
	</div><!--close .shop-table-container-->

	<div class="row">
		<div class="<?php echo sp_column_css( '12', '7', '8', '9' ); ?>">
			<?php
			// get saved values
			$image_upload = sp_get_option( 'checkout_secured_image' );
			$ssl_seal = sp_get_option( 'checkout_secured_code' );

			if ( isset( $ssl_seal ) && ! empty( $ssl_seal ) ) {
			?>
				<div class="checkout-ssl-seal">
					<?php echo apply_filters( 'sp_woo_checkout_secure_icon', '<p class="lock"><i class="icon-locked" aria-hidden="true"></i> ' . __( 'SECURED CHECKOUT', 'sp-theme' ) ) . '</p>'; ?>
					<?php echo $ssl_seal; ?>
				</div><!--close .checkout-ssl-seal-->
			<?php
			} else {
				if ( isset( $image_upload ) && ! empty( $image_upload ) )
					// check for ssl
					if ( is_ssl() )
						$image_upload = str_replace( 'http', 'https', $image_upload );
				?>
					<div class="checkout-ssl-seal">
						<?php echo apply_filters( 'sp_woo_checkout_secure_icon', '<p class="lock"><i class="icon-locked" aria-hidden="true"></i> ' . __( 'SECURED CHECKOUT', 'sp-theme' ) ) . '</p>'; ?>
						<img src="<?php echo esc_url( $image_upload ); ?>" alt="<?php esc_attr_e( 'SSL SEAL', 'sp-theme' ); ?>" />
					</div><!--close .checkout-ssl-seal-->
			<?php
			}
			?>
		</div><!--close .column-->

		<div class="<?php echo sp_column_css( '12', '5', '4', '3' ); ?>">
			<table class="summary">
				<tr class="cart-subtotal">
					<th><?php _e( 'Cart Subtotal', 'sp-theme' ); ?></th>
					<td><?php echo $woocommerce->cart->get_cart_subtotal(); ?></td>
				</tr>

				<?php if ( $woocommerce->cart->get_discounts_before_tax() ) : ?>

				<tr class="discount">
					<th><?php _e( 'Cart Discount', 'sp-theme' ); ?></th>
					<td>-<?php echo $woocommerce->cart->get_discounts_before_tax(); ?></td>
				</tr>

				<?php endif; ?>

				<?php if ( $woocommerce->cart->needs_shipping() && $woocommerce->cart->show_shipping() ) : ?>

					<?php do_action('woocommerce_review_order_before_shipping'); ?>

					<tr class="shipping">
						<th><?php _e( 'Shipping', 'sp-theme' ); ?></th>
						<td><?php woocommerce_get_template( 'cart/shipping-methods.php', array( 'available_methods' => $available_methods ) ); ?></td>
					</tr>

					<?php do_action('woocommerce_review_order_after_shipping'); ?>

				<?php endif; ?>

				<?php foreach ( $woocommerce->cart->get_fees() as $fee ) : ?>

					<tr class="fee fee-<?php echo $fee->id ?>">
						<th><?php echo $fee->name ?></th>
						<td><?php
							if ( $woocommerce->cart->tax_display_cart == 'excl' )
								echo woocommerce_price( $fee->amount );
							else
								echo woocommerce_price( $fee->amount + $fee->tax );
						?></td>
					</tr>

				<?php endforeach; ?>

				<?php
					// Show the tax row if showing prices exlcusive of tax only
					if ( $woocommerce->cart->tax_display_cart == 'excl' ) {
						foreach ( $woocommerce->cart->get_tax_totals() as $code => $tax ) {
							echo '<tr class="tax-rate tax-rate-' . $code . '">
								<th>' . $tax->label . '</th>
								<td>' . $tax->formatted_amount . '</td>
							</tr>';
						}
					}
				?>

				<?php if ( $woocommerce->cart->get_discounts_after_tax() ) : ?>

				<tr class="discount">
					<th><?php _e( 'Order Discount', 'sp-theme' ); ?></th>
					<td>-<?php echo $woocommerce->cart->get_discounts_after_tax(); ?></td>
				</tr>

				<?php endif; ?>

				<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

				<tr class="total">
					<th><strong><?php _e( 'Order Total', 'sp-theme' ); ?></strong></th>
					<td>
						<strong><?php echo $woocommerce->cart->get_total(); ?></strong>
						<?php
							// If prices are tax inclusive, show taxes here
							if ( $woocommerce->cart->tax_display_cart == 'incl' ) {
								$tax_string_array = array();

								foreach ( $woocommerce->cart->get_tax_totals() as $code => $tax ) {
									$tax_string_array[] = sprintf( '%s %s', $tax->formatted_amount, $tax->label );
								}

								if ( ! empty( $tax_string_array ) ) {
									?><small class="includes_tax"><?php printf( __( '(Includes %s)', 'sp-theme' ), implode( ', ', $tax_string_array ) ); ?></small><?php
								}
							}
						?>
					</td>
				</tr>

				<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>
			</table>
		</div><!--close .column-->
	</div><!--close .row-->

	<div id="payment">
		<?php if ($woocommerce->cart->needs_payment()) : ?>
		<ul class="payment_methods methods">
			<?php
				$available_gateways = $woocommerce->payment_gateways->get_available_payment_gateways();
				if ( ! empty( $available_gateways ) ) {

					// Chosen Method
					if ( isset( $woocommerce->session->chosen_payment_method ) && isset( $available_gateways[ $woocommerce->session->chosen_payment_method ] ) ) {
						$available_gateways[ $woocommerce->session->chosen_payment_method ]->set_current();
					} elseif ( isset( $available_gateways[ get_option( 'woocommerce_default_gateway' ) ] ) ) {
						$available_gateways[ get_option( 'woocommerce_default_gateway' ) ]->set_current();
					} else {
						current( $available_gateways )->set_current();
					}

					foreach ( $available_gateways as $gateway ) {
						?>
						<li>
							<input type="radio" id="payment_method_<?php echo $gateway->id; ?>" class="input-radio" name="payment_method" value="<?php echo esc_attr( $gateway->id ); ?>" <?php checked( $gateway->chosen, true ); ?> />
							<label for="payment_method_<?php echo $gateway->id; ?>"><?php echo $gateway->get_title(); ?> <?php echo $gateway->get_icon(); ?></label>
							<?php
								if ( $gateway->has_fields() || $gateway->get_description() ) :
									echo '<div class="payment_box payment_method_' . $gateway->id . '" ' . ( $gateway->chosen ? '' : 'style="display:none;"' ) . '>';
									$gateway->payment_fields();
									echo '</div>';
								endif;
							?>
						</li>
						<?php
					}
				} else {

					if ( ! $woocommerce->customer->get_country() )
						echo '<p>' . __( 'Please fill in your details above to see available payment methods.', 'sp-theme' ) . '</p>';
					else
						echo '<p>' . __( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'sp-theme' ) . '</p>';

				}
			?>
		</ul>
		<?php endif; ?>

		<div class="form-row place-order">

			<noscript><?php _e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'sp-theme' ); ?><br/><input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="<?php _e( 'Update totals', 'sp-theme' ); ?>" /></noscript>

			<?php $woocommerce->nonce_field('process_checkout')?>

			<?php do_action( 'woocommerce_review_order_before_submit' ); ?>
			
			<div class="row clearfix">
				<div class="pull-right">
				<?php
				$order_button_text = apply_filters('woocommerce_order_button_text', __( 'Place order', 'sp-theme' ));

				echo apply_filters('woocommerce_order_button_html', '<input type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="' . $order_button_text . '" />' );
				?>
					
				<?php do_action( 'woocommerce_review_order_after_submit' ); ?>
				</div><!--close .column-->

				<div class="pull-right">

				<?php if (woocommerce_get_page_id('terms')>0) : ?>
				<p class="form-row terms">
					<label for="terms" class="checkbox"><?php _e( 'I have read and accept the', 'sp-theme' ); ?> <a href="<?php echo esc_url( get_permalink(woocommerce_get_page_id('terms')) ); ?>" target="_blank"><?php _e( 'terms &amp; conditions', 'sp-theme' ); ?></a></label>
					<input type="checkbox" class="input-checkbox" name="terms" <?php checked( isset( $_POST['terms'] ), true ); ?> id="terms" />
				</p>
				<?php endif; ?>

				</div><!--close .column-->


			</div><!--close .row-->
		</div>

		<div class="clear"></div>

	</div>

</div>
