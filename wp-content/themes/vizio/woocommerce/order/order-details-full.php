<?php
/**
 * Order details
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.3
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

$order = new WC_Order( $order_id );
?>
<h3><?php _e( 'Order Details', 'sp-theme' ); ?></h3>

<ul class="order_details clearfix">
	<li class="order">
		<?php _e( 'Order:', 'sp-theme' ); ?>
		<strong><?php echo $order->get_order_number(); ?></strong>
	</li>
	<li class="date">
		<?php _e( 'Date:', 'sp-theme' ); ?>
		<strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
	</li>
	<li class="status">
		<?php _e( 'Status:', 'sp-theme' ); ?>
		<?php $status = get_term_by( 'slug', $order->status, 'shop_order_status' ); ?>
		<strong><?php printf( ucfirst( __( '%s', 'sp-theme' ) ), $status->name ); ?></strong>
	</li>
</ul>

<table class="shop_table order_details">
	<thead>
		<tr>
			<th class="product-image"></th>
			<th class="product-name"><?php _e( 'Product', 'sp-theme' ); ?></th>
			<th class="product-quantity"><?php _e( 'Quantity', 'sp-theme' ); ?></th>
			<th class="product-total"><?php _e( 'Total', 'sp-theme' ); ?></th>
		</tr>
	</thead>

	<tbody>
		<?php
		if (sizeof($order->get_items())>0) {

			foreach($order->get_items() as $item) {

				$_product = get_product( $item['variation_id'] ? $item['variation_id'] : $item['product_id'] );

				if ( is_object( $_product ) )
					$image = sp_get_image( get_post_thumbnail_id( $_product->id ), 60, 60, true );
				else
					$image = sp_get_image( get_post_thumbnail_id( 0 ), 60, 60, true );

				echo '
					<tr class = "' . esc_attr( apply_filters( 'woocommerce_order_table_item_class', 'order_table_item', $item, $order ) ) . '">
						<td class="product-image"><img src="' . esc_url( $image['url'] ) . '" alt="' . esc_attr( $image['alt'] ) . '" /></td>
						<td class="product-name">' .
							apply_filters( 'woocommerce_order_table_product_title', '<a href="' . get_permalink( $item['product_id'] ) . '">' . $item['name'] . '</a>', $item );

				// check if we need to display variations
				$show = false;
				foreach( $item['item_meta'] as $k => $v ) {
					if ( preg_match( '/^pa_/', $k ) ) {
						$show = true;
						break;
					}
				}

				if ( $show ) {
					$item_meta = new WC_Order_Item_Meta( $item['item_meta'] );
					$item_meta->display();
				}

				if ( $_product && $_product->exists() && $_product->is_downloadable() && $order->is_download_permitted() ) {

					$download_file_urls = $order->get_downloadable_file_urls( $item['product_id'], $item['variation_id'], $item );

					$i     = 0;
					$links = array();

					foreach ( $download_file_urls as $file_url => $download_file_url ) {

						$filename = woocommerce_get_filename_from_url( $file_url );

						$links[] = '<small><a href="' . $download_file_url . '"><i class="icon-download"></i> ' . sprintf( __( 'Download file%s', 'sp-theme' ), ( count( $download_file_urls ) > 1 ? ' ' . ( $i + 1 ) . ': ' : ': ' ) ) . $filename . '</a></small>';

						$i++;
					}

					echo implode( '<br />', $links );
				}

				echo '</td>';
				
				echo '<td class="product-quantity">' .
										apply_filters( 'woocommerce_checkout_item_quantity', '<strong class="product-quantity">' . $item['qty'] . '</strong>', $item ) . '</td>';

				echo '<td class="product-total">' . $order->get_formatted_line_subtotal( $item ) . '</td></tr>';
			
				// Show any purchase notes
				if ($order->status=='completed' || $order->status=='processing') {
					if ($purchase_note = get_post_meta( $_product->id, '_purchase_note', true))
						echo '<tr class="product-purchase-note"><td colspan="3">' . apply_filters('the_content', $purchase_note) . '</td></tr>';
				}

			}
		}

		do_action( 'woocommerce_order_items_table', $order );
		?>
	</tbody>
</table>

<div class="row">
	<div class="<?php echo sp_column_css( '', '', '', '4', '', '', '8' ); ?>">
		<table class="summary">
			<?php
				if ( $totals = $order->get_order_item_totals() ) foreach ( $totals as $total ) :
					?>
					<tr class="<?php echo strtolower( str_replace( ':', '', str_replace( ' ', '-', $total['label'] ) ) ); ?>">
						<th scope="row"><?php echo $total['label']; ?></th>
						<td><?php echo $total['value']; ?></td>
					</tr>
					<?php
				endforeach;
			?>
		</table>

		<?php if ( get_option('woocommerce_allow_customers_to_reorder') == 'yes' && $order->status=='completed' ) : ?>
			<p class="order-again">
				<a href="<?php echo esc_url( $woocommerce->nonce_url( 'order_again', add_query_arg( 'order_again', $order->id, add_query_arg( 'order', $order->id, get_permalink( woocommerce_get_page_id( 'view_order' ) ) ) ) ) ); ?>" class="button"><?php _e( 'Order Again', 'sp-theme' ); ?></a>
			</p>
		<?php endif; ?>

		<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>
	</div><!--close .column-->
</div><!--close .row-->

<header>
	<h3><?php _e( 'Customer details', 'sp-theme' ); ?></h3>
</header>
<dl class="customer_details">
<?php
	if ($order->billing_email) echo '<dt>'.__( 'Email:', 'sp-theme' ).'</dt><dd>'.$order->billing_email.'</dd>';
	if ($order->billing_phone) echo '<dt>'.__( 'Telephone:', 'sp-theme' ).'</dt><dd>'.$order->billing_phone.'</dd>';
?>
</dl>

<?php if (get_option('woocommerce_ship_to_billing_address_only')=='no') : ?>

<div class="row addresses">

	<div class="<?php echo sp_column_css( '', '', '', '6' ); ?>">

<?php endif; ?>

		<header class="title">
			<h3><?php _e( 'Billing Address', 'sp-theme' ); ?></h3>
		</header>
		<address><p>
			<?php
				if (!$order->get_formatted_billing_address()) _e( 'N/A', 'sp-theme' ); else echo $order->get_formatted_billing_address();
			?>
		</p></address>

<?php if (get_option('woocommerce_ship_to_billing_address_only')=='no') : ?>

	</div><!--close .column-->

	<div class="<?php echo sp_column_css( '', '', '', '6' ); ?>">

		<header class="title">
			<h3><?php _e( 'Shipping Address', 'sp-theme' ); ?></h3>
		</header>
		<address><p>
			<?php
				if (!$order->get_formatted_shipping_address()) _e( 'N/A', 'sp-theme' ); else echo $order->get_formatted_shipping_address();
			?>
		</p></address>

	</div><!--close .column-->

</div><!--close .row-->

<?php endif; ?>
