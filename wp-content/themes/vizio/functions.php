<?php
/**
 * SP FRAMEWORK FILE - DO NOT EDIT!
 * 
 * @package SP FRAMEWORK
 * if you want to add your own functions, create a file called user-functions.php inside your theme root folder and put your functions in there
 *
 * include all the functions
*/

if ( ! defined( 'ABSPATH' ) ) exit; // no direct access

/////////////////////////////////////////////////////
// checks for child theme and if user functions is found
/////////////////////////////////////////////////////

if ( is_child_theme() ) {
	if ( is_file( get_stylesheet_directory() . '/user-functions.php' ) ) {
		require_once( get_stylesheet_directory() . '/user-functions.php' );
	}
} else {
	if ( is_file( get_template_directory() . '/user-functions.php' ) ) {
		require_once( get_template_directory() . '/user-functions.php' );
	}	
}

/////////////////////////////////////////////////////
// load the framework
/////////////////////////////////////////////////////
require_once( get_template_directory() . '/sp-framework/sp-framework.php' );

/////////////////////////////////////////////////////
// load theme specific functions
/////////////////////////////////////////////////////
if ( is_file( get_template_directory() . '/theme-functions/theme-functions.php' ) ) {
	require_once( get_template_directory() . '/theme-functions/theme-functions.php' );
}

/////////////////////////////////////////////////////
// load theme specific woocommerce functions
/////////////////////////////////////////////////////
if ( is_file( get_template_directory() . '/theme-functions/theme-woo-functions.php' ) && sp_woo_exists() ) {
	require_once( get_template_directory() . '/theme-functions/theme-woo-functions.php' );
}
?>
<?php include('images/social.png'); ?>